import UIKit

class ThirdViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonSettings()
    }
    
    func buttonSettings(){
        backButton.layer.cornerRadius = 10
        backButton.backgroundColor = .systemCyan
    }
    
    @IBAction func didPressedButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}


