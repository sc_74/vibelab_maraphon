package com.example.project_activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        val age = intent.getIntExtra("Age", 0)
        findViewById<TextView>(R.id.AgeText).text = "Your age is $age"
        val mText = findViewById<TextView>(R.id.MainText)
        when (age) {
            in 1..17 -> mText.setText(R.string.child)
            in 18..100 -> mText.setText(R.string.adult)
            else -> mText.setText(R.string.incorrect)
        }
    }
}
