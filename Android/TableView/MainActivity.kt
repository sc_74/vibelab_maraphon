package com.example.tableview_project

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    var last = 0
    var random_number = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val image: ImageView = findViewById(R.id.Image)
        val button: Button = findViewById(R.id.Button)
        button.setOnClickListener {
            while (random_number == last){
                random_number = Random.nextInt(1,5)
            }
            when(random_number){
                1 -> image.setImageDrawable(getDrawable(R.drawable.image_1))
                2 -> image.setImageDrawable(getDrawable(R.drawable.image_2))
                3 -> image.setImageDrawable(getDrawable(R.drawable.image_3))
                4 -> image.setImageDrawable(getDrawable(R.drawable.image_4))
            }
            last = random_number
            val color = Color.argb(255,
                Random.nextInt(0,255),
                Random.nextInt(0,255),
                Random.nextInt(0,255))
            button.setBackgroundColor(color)
        }
    }
}
