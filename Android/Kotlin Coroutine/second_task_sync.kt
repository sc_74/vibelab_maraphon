import kotlinx.coroutines.*
import kotlin.random.Random

suspend fun First_Number():Int
{
    delay(500L)
    return Random.nextInt(0,100)

}
suspend fun Second_Number():Int
{
    delay(750L)
    return Random.nextInt(0,100)
}

suspend fun main()
{
    val begin = System.currentTimeMillis();
    val result = First_Number() + Second_Number()
    val end = System.currentTimeMillis();
    println("Result is $result. Elapsed time in milliseconds: ${end-begin}")
}



