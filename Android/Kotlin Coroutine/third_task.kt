import kotlinx.coroutines.*
import kotlin.random.Random

suspend fun First_Number():Int
{
    delay(500L)
    return Random.nextInt(0,100)

}
suspend fun Second_Number():Int
{
    delay(750L)
    return Random.nextInt(0,100)
}

suspend fun main() = coroutineScope{
    val begin = System.currentTimeMillis();
    print("I'm sleeping 0 ...")
    delay(1000L)
    print(" I'm sleeping 1 ...")
    delay(1000L)
    print("  I'm sleeping 2 ...\n")
    delay(1000L)
    launch { println("main: I'm tired of waiting! I'm running finally") }
    val First_num_async: Deferred<Int> = async {First_Number()}
    val Second_num_async: Deferred<Int> = async{Second_Number()}
    val result = First_num_async.await() + Second_num_async.await()
    val end = System.currentTimeMillis();
    println("Result is $result. Elapsed time in milliseconds: ${end-begin}")
    println("main: Now I can quit.»")
}






